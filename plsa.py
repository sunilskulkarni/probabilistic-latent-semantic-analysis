import numpy as np
import math
import re
import codecs

def normalize_old(input_matrix):
    """
    Normalizes the rows of a 2d input_matrix so they sum to 1
    """

    row_sums = input_matrix.sum(axis=1)
    assert (np.count_nonzero(row_sums) == np.shape(row_sums)[0])  # no row should sum to zero
    new_matrix = input_matrix / row_sums[:, np.newaxis]
    return new_matrix

def normalize(vec):
    s = sum(vec)
    assert (abs(s) != 0.0)  # the sum must not be 0
    """
    if abs(s) < 1e-6:
        print "Sum of vectors sums almost to 0. Stop here."
        print "Vec: " + str(vec) + " Sum: " + str(s)
        assert(0) # assertion fails
    """

    for i in range(len(vec)):
        assert (vec[i] >= 0)  # element must be >= 0
        vec[i] = vec[i] * 1.0 / s

STOP_WORDS_SET = set()

class Document(object):
    '''
    Splits a text file into an ordered list of words.
    '''

    # List of punctuation characters to scrub. Omits, the single apostrophe,
    # which is handled separately so as to retain contractions.
    PUNCTUATION = ['(', ')', ':', ';', ',', '-', '!', '.', '?', '/', '"', '*']

    # Carriage return strings, on *nix and windows.
    CARRIAGE_RETURNS = ['\n', '\r\n']

    # Final sanity-check regex to run on words before they get
    # pushed onto the core words list.
    WORD_REGEX = "^[a-z']+$"

    def __init__(self):
        '''
        Set source file location, build contractions list, and initialize empty
        lists for lines and words.
        '''

        # self.file = open(self.filepath)
        self.lines = []
        self.words = []



    def split(self, words, STOP_WORDS_SET):
        '''
        Split file into an ordered list of words. Scrub out punctuation;
        lowercase everything; preserve contractions; disallow strings that
        include non-letters.
        '''


        for word in words:
            clean_word = self._clean_word(word)
            if clean_word and (clean_word not in STOP_WORDS_SET) and (len(clean_word) > 1):  # omit stop words
                self.words.append(clean_word)

    def _clean_word(self, word):
        '''
        Parses a space-delimited string from the text and determines whether or
        not it is a valid word. Scrubs punctuation, retains contraction
        apostrophes. If cleaned word passes final regex, returns the word;
        otherwise, returns None.
        '''
        word = word.lower()
        for punc in Document.PUNCTUATION + Document.CARRIAGE_RETURNS:
            word = word.replace(punc, '').strip("'")
        return word if re.match(Document.WORD_REGEX, word) else None



# class Corpus(object):
#     """
#     A collection of documents.
#     """
#
#     def __init__(self, documents_path):
#         """
#         Initialize empty document list.
#         """
#         self.documents = []
#         self.vocabulary = []
#         self.likelihoods = []
#         self.documents_path = documents_path
#         self.term_doc_matrix = None
#         self.document_topic_prob = None  # P(z | d)
#         self.topic_word_prob = None  # P(w | z)
#         self.topic_prob = None  # P(z | d, w)
#
#         self.number_of_documents = 0
#         self.vocabulary_size = 0
#
#     def build_corpus(self):
#         eachDoc = []
#         with open(self.documents_path) as fp:
#             for line in fp:
#                 eachDoc = line.split()
#                 self.documents.append(eachDoc)
#         self.number_of_documents = len(self.documents)
#
#     def build_vocabulary(self):
#
#         data = ""
#         gg = ""
#         final = []
#         with open(self.documents_path, 'r') as file:
#             data = file.read().replace('\n', '')
#             gg = re.sub('\d', '', data)
#         unique_words = set(gg.split())
#         final = list(unique_words)
#         self.vocabulary = final
#         self.vocabulary_size = len(self.vocabulary)
#
#     def build_term_doc_matrix(self):
#         status = {}
#         for x in range(0, len(self.documents)):
#             status[x] = {}
#             for y in range(0, len(self.vocabulary)):
#                 status[x][y] = self.documents[x].count(self.vocabulary[y])
#         self.term_doc_matrix = status
#         print(self.term_doc_matrix)
#
#     def initialize_randomly(self, number_of_topics):
#         self.document_topic_prob = np.random.random(size=(self.number_of_documents, number_of_topics))
#         self.document_topic_prob = normalize(self.document_topic_prob)
#         self.topic_word_prob = np.random.random(size=(number_of_topics, len(self.vocabulary)))
#         self.topic_word_prob = normalize(self.topic_word_prob)
#
#         pass  # REMOVE THIS
#
#     def initialize_uniformly(self, number_of_topics):
#         """
#         Initializes the matrices: self.document_topic_prob and self.topic_word_prob with a uniform
#         probability distribution. This is used for testing purposes.
#
#         DO NOT CHANGE THIS FUNCTION
#         """
#         self.document_topic_prob = np.ones((self.number_of_documents, number_of_topics))
#         self.document_topic_prob = normalize(self.document_topic_prob)
#
#         self.topic_word_prob = np.ones((number_of_topics, len(self.vocabulary)))
#         self.topic_word_prob = normalize(self.topic_word_prob)
#
#     def initialize(self, number_of_topics, random=False):
#         """ Call the functions to initialize the matrices document_topic_prob and topic_word_prob
#         """
#         print("Initializing...")
#
#         if random:
#             self.initialize_randomly(number_of_topics)
#         else:
#             self.initialize_uniformly(number_of_topics)
#
#     def expectation_step(self):
#         """ The E-step updates P(z | w, d)
#         """
#         print("E step:")
#
#         for d in range(self.number_of_documents):
#             for w in range(self.vocabulary_size):
#                 prob = self.document_topic_prob[d, :] * self.topic_word_prob[:, w]
#                 prob = normalize(prob[np.newaxis, :])
#                 self.topic_prob[d, :, w] = prob
#             self.topic_prob = normalize(self.topic_prob[d, :, :])
#
#     def maximization_step(self, number_of_topics):
#         """ The M-step updates P(w | z)
#         """
#         print("M step:")
#
#
#         # update P(w | z)
#         for z in range(number_of_topics):
#             for w in range(self.vocabulary_size):
#                 sum = 0
#                 for d in range(self.number_of_documents):
#                     sum = sum + self.term_doc_matrix[d][w] * self.topic_prob[d, z, w]
#                 self.topic_word_prob[z][w] = sum
#         self.topic_word_prob = normalize(self.topic_word_prob)
#
#         # update P(z | d)
#         for d in range(self.number_of_documents):
#             for z in range(number_of_topics):
#                 sum = 0
#                 for w in range(self.vocabulary_size):
#                     sum = sum + self.term_doc_matrix[d][w] * self.topic_prob[d, z, w]
#                     print(self.topic_prob[d, z, w])
#                 self.document_topic_prob[d][z] = sum
#         self.document_topic_prob = normalize(self.document_topic_prob)
#
#
#     def calculate_likelihood(self, number_of_topics):
#         doc = 0
#         for d_index in range(len(self.documents)):
#             for z in range(number_of_topics):
#                 total = 0
#                 for w_index in range(self.vocabulary_size):
#                     total += self.topic_prob[d_index, z, w_index] * self.topic_word_prob[z, w_index]
#                 total += np.log(total) * self.term_doc_matrix[d_index][w_index]
#             doc += total
#
#         self.likelihoods = doc
#         return doc
#
#     def plsa(self, number_of_topics, max_iter, epsilon):
#
#         """
#         Model topics.
#         """
#         print("EM iteration begins...")
#
#         # build term-doc matrix
#         self.build_term_doc_matrix()
#
#         # Create the counter arrays.
#
#         # P(z | d, w)
#         self.topic_prob = np.zeros([self.number_of_documents, number_of_topics, self.vocabulary_size], dtype=np.float)
#
#         # P(z | d) P(w | z)
#         self.initialize(number_of_topics, random=True)
#
#         # Run the EM algorithm
#         current_likelihood = 0.0
#
#         for iteration in range(max_iter):
#             print("Iteration #" + str(iteration + 1) + "...")
#
#             self.expectation_step()
#             self.maximization_step(number_of_topics)
#             self.calculate_likelihood(number_of_topics)
#             current_likelihood = self.likelihoods[len(self.likelihoods) - 1]
#             print(current_likelihood)
#             if (abs(current_likelihood - self.likelihoods) <= epsilon):
#                 break
#
#

class Corpus(object):
    """
    A collection of documents.
    """

    def __init__(self, documents_path):
        """
        Initialize empty document list.
        """
        self.documents = []
        self.vocabulary = []
        self.likelihoods = []
        self.documents_path = documents_path
        self.term_doc_matrix = None
        self.document_topic_prob = None  # P(z | d)
        self.topic_word_prob = None  # P(w | z)
        self.topic_prob = None  # P(z | d, w)

        self.number_of_documents = 0
        self.vocabulary_size = 0



    def add_document(self, document):
        '''
        Add a document to the corpus.
        '''
        self.documents.append(document)

    def build_corpus(self):
        file = codecs.open(self.documents_path, 'r', 'utf-8')
        lines = [re.sub(r'\d+', '', document.strip()).split() for document in file]
        # print(len(self.documents))
        file.close()
        for words in lines:
            document = Document()  # instantiate document
            document.split(words, STOP_WORDS_SET)  # tokenize
            self.add_document(document)  # push onto corpus documents list

        self.number_of_documents = len(self.documents)

    def build_vocabulary(self):

        discrete_set = set()
        for document in self.documents:
            for word in document.words:
                discrete_set.add(word)
        self.vocabulary = list(discrete_set)
        self.vocabulary_size = len(self.vocabulary)


    def build_term_doc_matrix(self):
        self.term_doc_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.int)
        for d_index, doc in enumerate(self.documents):
            term_count = np.zeros(self.vocabulary_size, dtype=np.int)
            for word in doc.words:
                if word in self.vocabulary:
                    w_index = self.vocabulary.index(word)
                    term_count[w_index] = term_count[w_index] + 1
            self.term_doc_matrix[d_index] = term_count
        #print(self.term_doc_matrix)


    def initialize_randomly(self, number_of_topics):
        self.document_topic_prob = np.random.random(size=(self.number_of_documents, number_of_topics))
        # print('Before normalize: document_topic_prob..')
        # print(self.document_topic_prob)
        for d_index in range(len(self.documents)):
            normalize(self.document_topic_prob[d_index])  # normalize for each document
        print('After normalize: document_topic_prob..')
        print(self.document_topic_prob)

        self.topic_word_prob = np.random.random(size=(number_of_topics, len(self.vocabulary)))
        # print('Before topic_word_prob..')
        # print(self.topic_word_prob)

        for z in range(number_of_topics):
            normalize(self.topic_word_prob[z])  # normalize for each topic


        print('After topic_word_prob..')
        print(self.topic_word_prob)


    def initialize_uniformly(self, number_of_topics):
        """
        Initializes the matrices: self.document_topic_prob and self.topic_word_prob with a uniform
        probability distribution. This is used for testing purposes.

        DO NOT CHANGE THIS FUNCTION
        """
        self.document_topic_prob = np.ones((self.number_of_documents, number_of_topics))
        self.document_topic_prob = normalize(self.document_topic_prob)

        self.topic_word_prob = np.ones((number_of_topics, len(self.vocabulary)))
        self.topic_word_prob = normalize(self.topic_word_prob)

    def initialize(self, number_of_topics, random=False):
        """ Call the functions to initialize the matrices document_topic_prob and topic_word_prob
        """
        print("Initializing...")

        if random:
            self.initialize_randomly(number_of_topics)
        else:
            self.initialize_uniformly(number_of_topics)

    def expectation_step(self):
        """ The E-step updates P(z | w, d)
        """
        print("E step:")

        for d_index, document in enumerate(self.documents):
            for w_index in range(self.vocabulary_size):

                prob = self.document_topic_prob[d_index, :] * self.topic_word_prob[:, w_index]
                if sum(prob) == 0.0:
                    print("d_index = " + str(d_index) + ",  w_index = " + str(w_index))
                    print("self.document_topic_prob[d_index, :] = " + str(self.document_topic_prob[d_index, :]))
                    print("self.topic_word_prob[:, w_index] = " + str(self.topic_word_prob[:, w_index]))
                    print("topic_prob[d_index][w_index] = " + str(prob))
                    exit(0)
                else:
                    normalize(prob)
                self.topic_prob[d_index][w_index] = prob
        print('topic_prob after E-step..')
        print(self.topic_prob)
        print('-----')

    def maximization_step(self, number_of_topics):
        """ The M-step updates P(w | z)
        """
        print("M step:")

        for z in range(number_of_topics):
            for w_index in range(self.vocabulary_size):
                s = 0
                for d_index in range(len(self.documents)):
                    count = self.term_doc_matrix[d_index][w_index]
                    s = s + count * self.topic_prob[d_index, w_index, z]
                self.topic_word_prob[z][w_index] = s
            normalize(self.topic_word_prob[z])

        # update P(z | d)
        for d_index in range(len(self.documents)):
            for z in range(number_of_topics):
                s = 0
                for w_index in range(self.vocabulary_size):
                    count = self.term_doc_matrix[d_index][w_index]
                    s = s + count * self.topic_prob[d_index, w_index, z]
                self.document_topic_prob[d_index][z] = s
            #                print self.document_topic_prob[d_index]
            #                assert(sum(self.document_topic_prob[d_index]) != 0)
            normalize(self.document_topic_prob[d_index])

    def calculate_likelihood(self, number_of_topics):
        doc = 0
        for d_index in range(len(self.documents)):
            for z in range(number_of_topics):
                total = 0
                for w_index in range(self.vocabulary_size):
                    total += self.topic_prob[d_index, w_index, z] * self.topic_word_prob[z, w_index]
                total += np.log(total) * self.term_doc_matrix[d_index][w_index]
        doc += total
        self.likelihoods.append(doc)

        return doc

    def plsa(self, number_of_topics, max_iter, epsilon):

        """
        Model topics.
        """
        print("EM iteration begins...")

        # build term-doc matrix
        self.build_term_doc_matrix()

        # Create the counter arrays.

        # P(z | d, w)
        #self.topic_prob = np.zeros([self.number_of_documents, number_of_topics, self.vocabulary_size], dtype=np.float)


        # P(z | d) P(w | z)
        #self.initialize(number_of_topics, random=True)
        self.document_topic_prob = np.zeros([self.number_of_documents, number_of_topics], dtype=np.float)  # P(z | d)
        self.topic_word_prob = np.zeros([number_of_topics, len(self.vocabulary)], dtype=np.float)  # P(w | z)
        self.topic_prob = np.zeros([self.number_of_documents, len(self.vocabulary), number_of_topics],dtype=np.float)  # P(z | d, w)



        # Initialize

        # randomly assign values
        self.initialize(number_of_topics, random=True)

        # Run the EM algorithm
        current_likelihood = 0.0

        for iteration in range(max_iter):
            print("Iteration #" + str(iteration + 1) + "...")

            self.expectation_step()
            self.maximization_step(number_of_topics)

            current_likelihood = self.calculate_likelihood(number_of_topics)
            previous_likelihood = self.likelihoods[iteration - 1]
            print('Current Likelihood:' + str(current_likelihood))
            print('Previous likelihood:' + str(previous_likelihood))


            if (abs(current_likelihood - previous_likelihood) <= epsilon and iteration != 0):
                break

        print('End of plsa..')
        print(self.document_topic_prob)


def main():
    documents_path = 'data/test1.txt'
    corpus = Corpus(documents_path=documents_path)  # instantiate corpus
    corpus.build_corpus()
    print("Number of documents:" + str(len(corpus.documents)))

    corpus.build_vocabulary()
    print(corpus.vocabulary)
    print("Vocabulary size:" + str(len(corpus.vocabulary)))

    number_of_topics = 2
    max_iterations = 50
    epsilon = 0.001

    corpus.plsa(number_of_topics, max_iterations, epsilon)


if __name__ == '__main__':
    main()
